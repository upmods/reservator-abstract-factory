<?php
/*
* Abstract class Ticket. OOP design pattern Factory.
*/
abstract class ATicket
{
    public $ticket_id;
    public $order_id;    
    public $event_id;
    public $type;
    
    public function build(){
        echo '['.$this->type.'] creating object for event #'.$this->event_id;
        echo '<br />';
    }
    
    public function buildTicket(){
        echo '['.$this->type.'] reserving ticket #'.$this->ticket_id;
        echo '<br />';        
    }    

    public function buildOrder(){
        echo '['.$this->type.'] orderId #'.$this->order_id.' created';
        echo '<br />';        
    }

    public function notification(){
        echo '['.$this->type.'] Sending admin notification: Order #'.$this->order_id.' created for event #'.$this->ticket_id;          
    }
}

/*
* Local Reservation Ticket
*/
class Reservator extends ATicket
{
    public $type = 'local';
    
    public function build($event_id)
    {
        $this->event_id = (int)$event_id;    
        parent::build();
    }
    
    public function buildTicket()
    {
        $this->ticket_id = rand(99, 3000);    
        parent::buildTicket();
    }
    
    public function buildOrder()
    {
        $this->order_id = rand(99, 3000);
        parent::buildOrder();
    }
    
    public function notification()
    {
        parent::notification();
    }

}

/*
* Partner Reservation Ticket
*/
class ReservatorApiPartner extends ATicket
{
    public $type = 'partnerAPI';
    
    public function build($event_id)
    {
        $this->event_id = (int)$event_id;    
        parent::build();
    }
    
    public function buildTicket()
    {
        $this->ticket_id = rand(80000, 90000);    
        parent::buildTicket();
    }
    
    public function buildOrder()
    {
        $this->order_id = rand(80000, 90000);
        parent::buildOrder();
        $this->apiCall();
    }
    
    public function notification()
    {
        parent::notification();
    }
    
    public function apiCall()
    { 
        echo '['.$this->type.'] reserving ticket #'.$this->ticket_id.' VIA PARTNER API CALL';
        echo '<br />';
    }    

}

/*
* Factory Reservation Ticket
*/
class ReservatorFactory
{
   protected $type;
   protected $oticket;
   
   public function reserveRandomTicket($event)
   {
        $class_name = ((int)$event % 2)? 'Reservator' : 'ReservatorApiPartner';
        
        $this->oticket = new $class_name;
        $this->oticket->build($event);
        $this->oticket->buildTicket();
        $this->oticket->buildOrder();
        $this->oticket->notification();
        
        return $this->oticket;
   }   
}

